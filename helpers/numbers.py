import functools
import operator

from .sequences import Primes

def gcd(num_1, num_2):
    num_1 = abs(num_1)
    num_2 = abs(num_2)
    max = num_1 if num_1 > num_2 else num_2
    min = num_2 if max == num_1 else num_1
    quotient = int(max / min)
    remainder = max - quotient * min

    while remainder != 0:
        max = min
        min = remainder
        quotient = int(max / min)
        remainder = max - quotient * min

    return min


def factor(num):
    if num == 0:
        return None

    result = []
    if num < 0:
        result.append(-1)
        num *= -1
    if num == 1:
        result.append(1)
        return result

    # now num is at least 2
    while num != 1:
        prime = Primes()
        while num % prime.get() != 0:
            prime.next()
        num /= prime.get()
        result.append(prime.get())

    return result
    
def lcm(numbers):
    multiples = [] 
    for i in numbers:
        factors = factor(i)
        for multiple in multiples:
            if multiple in factors:
                factors.remove(multiple)
        for fact in factors:
            multiples.append(fact)

    return functools.reduce(operator.mul, multiples, 1)
