import math

class Fibonacci(object):
    def __init__(self, bufsize=3):
        self.__buffer = [1, 1]
        self.__cur = 0
        self.count = 0

        if bufsize < 3:
            raise BufferError("Buffer size must be at least 3")
        for i in range(2, bufsize):
            self.__buffer.append(0)

    def __str__(self):
        return "fibonacci #{} is {}".format(self.count, self.__buffer[self.__cur])

    def get(self):
        return self.__buffer[self.__cur]

    def next(self):
        self.__cur = (self.__cur + 1) % len(self.__buffer)
        self.count += 1
        if self.count > 1:
            self.__buffer[self.__cur] = \
                self.__buffer[(self.__cur - 1) % len(self.__buffer)] + \
                self.__buffer[(self.__cur - 2) % len(self.__buffer)]
        return self.get()

    def get_nth_num_ago(self, n):
        if n > len(self.__buffer) - 1 or n < 0:
            raise BufferError("invalid buffer index")
        return self.__buffer[(self.__cur - n) % len(self.__buffer)]

class Primes(object):
    def __init__(self):
        self.count = 0
        self.prime = 2

    def __str__(self):
        return "prime #{} is {}".format(self.count, self.prime)

    def get(self):
        return self.prime

    def next(self):
        self.prime = Primes.next_prime(self.prime)
        self.count += 1
        return self.prime

    @staticmethod
    def is_prime(num):
        if num <= 1:
            return False

        for i in range(2, int(math.sqrt(num)) + 1):
            if num % i == 0:
                return False
        return True

    @staticmethod
    def next_prime(num):
        cur = num + 1 if num > 0 else 2
        while not Primes.is_prime(cur):
            cur += 1
        return cur

