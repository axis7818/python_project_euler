import os

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
INPUT_DIR = os.path.join(ROOT_DIR, 'input_files')

def get_input_file(problem_number):
    return open(os.path.join(INPUT_DIR, '{}.in'.format(problem_number)))

