# euler.py
# Cameron Taylor
#
# This is the entry point for my project euler problems
#
# Usage: python euler.py [PROBLEM_NUMBER]
#   PROBLEM_NUMBER: the project euler problem number

import os
import requests
import time

from BeautifulSoup import BeautifulSoup
from os import sys

CUR_DIR = os.getcwd()

def print_usage():
    print("Usage: python euler.py [PROBLEM_NUMBER]")
    print("run 'python euler.py help' for more information")

def print_help():
    print("""Run 'python euler.py [COMMAND]'
COMMAND can be:
    [PROBLEM_NUMBER]: to run the desired problem
    help: for the help menu
    problem [PROBLEM_NUMBER]: to display the instructions for a given problem
    clearcache: to clear the problem instruction cache files""")

def print_problem(num):
    if num < 1:
        print("invalid problem number")
        return

    instruction_dir = os.path.join(CUR_DIR, 'instructions')
    if not os.path.exists(instruction_dir):
        os.mkdir(instruction_dir)    

    file_path = os.path.join(CUR_DIR, 'instructions', "{}.html".format(num))

    if not os.path.exists(file_path):
        page = requests.get("https://projecteuler.net/problem={}".format(num), verify=False)
        page_scraper = BeautifulSoup(page.text)
        instructions = page_scraper.find('div', {'class':'problem_content'})
        meta = page_scraper.find('div', {'id':'problem_info'}).span
        with open(file_path, 'w') as file:
            file.write(str(instructions))
            file.write("<br>")
            file.write(str(meta))

    os.system("html2text {}".format(file_path))

def clear_instruction_cache():
    print("clearing . . .")
    dir = os.path.join(CUR_DIR, 'instructions')
    for file in os.listdir(dir):
        path = os.path.join(dir, file)
        if os.path.isfile(path):
            os.unlink(path)
    print("Problem instruction cache was cleared")

def main():
    action = ""
    num_ndx = 1

    # print the correct usage
    if len(sys.argv) < 2:
        print_usage()
        exit()

    if sys.argv[1] == "help":
        print_help()
        exit()
    elif sys.argv[1] == "clearcache":
        clear_instruction_cache()
        exit()
    elif sys.argv[1] == "problem":
        if len(sys.argv) < 3:
            print_usage()
            exit()
        action = "instructions"
        num_ndx = 2

    # Get PROBLEM_NUMBER
    try:
        PROBLEM_NUMBER = int(sys.argv[num_ndx])
    except ValueError:
        PROBLEM_NUMBER = 0
    if PROBLEM_NUMBER < 1:
        print("{} is not a valid problem number".format(sys.argv[num_ndx]))
        exit()

    # execute action
    if action is "instructions":
        print_problem(PROBLEM_NUMBER)
    else:
        PROBLEM_FILE = os.path.join(CUR_DIR, 'solutions', "{}.py".format(PROBLEM_NUMBER))
        if not os.path.exists(PROBLEM_FILE):
            print("problem {} has not been implemented.".format(PROBLEM_NUMBER))
            exit()
        file_name = PROBLEM_FILE[0:len(PROBLEM_FILE) - 3] # strip the .py off the end

        start = time.time()
        os.system("python -m {}".format(file_name))
        end = time.time()
        print("\ntime (in seconds): {}".format(end - start))

main()
