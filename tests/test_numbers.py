import sys, os
sys.path.insert(0, os.path.dirname(os.getcwd()))

import unittest

from helpers import numbers

class NumbersTestCase(unittest.TestCase):
    def test_gcd(self):
        self.assertEqual(numbers.gcd(28, 12), 4)
        self.assertEqual(numbers.gcd(12, 32), 4)
        self.assertEqual(numbers.gcd(29, 7), 1)
        self.assertEqual(numbers.gcd(-7, 29), 1)
        self.assertEqual(numbers.gcd(-7, -29), 1)

    def test_factor(self):
        self.assertIsNone(numbers.factor(0))
        self.assertEqual(numbers.factor(1), [1])
        self.assertEqual(numbers.factor(-1), [-1, 1])
        self.assertEqual(numbers.factor(6), [2, 3])
        self.assertEqual(numbers.factor(13195), [5, 7, 13, 29])

    def test_lcm(self):
        self.assertEqual(numbers.lcm(range(1, 11)), 2520)
        self.assertEqual(numbers.lcm(range(1, 4)), 6)
        self.assertEqual(numbers.lcm([2, 6]), 6)

if __name__ == "__main__":
    unittest.main()
