import sys, os
sys.path.insert(0, os.path.dirname(os.getcwd()))

import unittest
from helpers.sequences import Fibonacci, Primes

class FibonacciTestCase(unittest.TestCase):
    first_ten_fibs = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]

    def test_first_ten_values(self):
        fib = Fibonacci()
        result = []
        for i in range(0, 10):
            result.append(fib.get())
            fib.next()
        self.assertEqual(result, self.first_ten_fibs)
        self.assertEqual(fib.count, 10)
        self.assertEqual(str(fib), "fibonacci #10 is 89")

    def test_first_ten_values_with_bufsizes_4_to_100(self):
        for i in range(4, 101):
            fib = Fibonacci(bufsize=i)
            result = []
            for j in range(0, 10):
                result.append(fib.get())
                fib.next()
            self.assertEqual(result, self.first_ten_fibs)
            self.assertEqual(fib.count, 10)
            self.assertEqual(str(fib), "fibonacci #10 is 89")

    def test_invalid_buffer_sizes(self):
        self.assertRaises(BufferError, Fibonacci, bufsize=-2)
        self.assertRaises(BufferError, Fibonacci, bufsize=2)
        Fibonacci(bufsize=3)
        Fibonacci(bufsize=6)
        Fibonacci(bufsize=100)

class PrimeTestCase(unittest.TestCase):
    def test_is_prime(self):
        self.assertFalse(Primes.is_prime(-3))
        self.assertFalse(Primes.is_prime(0))
        self.assertFalse(Primes.is_prime(1))
        self.assertTrue(Primes.is_prime(2))
        self.assertTrue(Primes.is_prime(3))
        self.assertFalse(Primes.is_prime(4))
        self.assertTrue(Primes.is_prime(5))
        self.assertFalse(Primes.is_prime(6))
        self.assertTrue(Primes.is_prime(7))
        self.assertFalse(Primes.is_prime(8))
        self.assertFalse(Primes.is_prime(9))
        self.assertFalse(Primes.is_prime(10))

    def test_next_prime(self):
        self.assertEqual(Primes.next_prime(-10), 2)
        self.assertEqual(Primes.next_prime(0), 2)
        self.assertEqual(Primes.next_prime(1), 2)
        self.assertEqual(Primes.next_prime(2), 3)
        self.assertEqual(Primes.next_prime(3), 5)
        self.assertEqual(Primes.next_prime(23), 29)
        self.assertEqual(Primes.next_prime(18), 19)
    
    def test_primes_to_string(self):
        primes = Primes()
        for i in range(0, 5):
            primes.next()
        self.assertEqual(str(primes), "prime #5 is 13")

if __name__ == '__main__':
    unittest.main()
