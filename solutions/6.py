import functools

sum_of_squares = functools.reduce(lambda x, y: x + y ** 2, range(1, 101))
square_of_sum = functools.reduce(lambda x, y: x + y, range(1, 101)) ** 2
print(square_of_sum - sum_of_squares)
