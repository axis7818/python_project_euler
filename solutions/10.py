from helpers.sequences import Primes

max_val = 2000000

print("this may take a couple minutes...")
total = 0
prime = Primes()
while prime.get() < max_val:
    total += prime.get()
    prime.next()

print(total)
