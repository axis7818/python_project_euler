from helpers.settings import get_input_file

size = (20, 20)

adj_len = 4

grid = []
file = get_input_file(11)
for line in file:
    nums = line.split()
    temp = []
    for num in nums:
        temp.append(int(num))
    grid.append(temp)
file.close()
       
# Print the data
print(grid[2][3])
if False:
    for row in grid:
        print(row)

def prod_horizontal(col, row):
    result = 1
    for i in range(0, adj_len):
        result *= grid[row][col + i]
    return result

def prod_vertical(col, row):
    result = 1
    for i in range(0, adj_len):
        result *= grid[row + i][col]
    return result

def prod_diagonal(col, row):
    result = 1
    for i in range(0, adj_len):
        result *= grid[row + i][col + i]
    return result

def other_prod_diagonal(col, row):
    result = 1
    for i in range(0, adj_len):
        result *= grid[row - i][col + i]
    return result

result = 0
for row in range(0, size[0]):
    for col in range(0, size[1]):
        # check horizontal
        if col + adj_len <= size[1]:
            temp = prod_horizontal(col, row)
            result = result if result > temp else temp
        # check vertical
        if row + adj_len <= size[0]:
            temp = prod_vertical(col, row)
            result = result if result > temp else temp
        # check diagonal
        if col + adj_len <= size[1] and row + adj_len <= size[0]:
            temp = prod_diagonal(col, row)
            result = result if result > temp else temp
        # check other diagonal
        if row >= adj_len - 1 and col + adj_len <= size[1]:
            temp = other_prod_diagonal(col, row)
            result = result if result > temp else temp

print(result)
