def is_palindrome(num):
    return str(num) == str(num)[::-1]

maxSoFar = 0
for i in range(100, 1000):
    for j in range(100, 1000):
        prod = i * j
        if is_palindrome(prod) and prod > maxSoFar:
            maxSoFar = prod

print(maxSoFar)
