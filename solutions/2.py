from helpers.sequences import Fibonacci

fib = Fibonacci()
sum = 0

while fib.get() < 4000000:
    if fib.get() % 2 == 0:
        sum += fib.get()
    fib.next()

print(sum)
