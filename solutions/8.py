import functools

from helpers.settings import get_input_file

file = get_input_file(8)
number = []

for line in file:
    for digit in line[:len(line) - 1]:
        number.append(int(digit))
file.close()

window = 13

max = functools.reduce(lambda x, y: x * y, number[:window], 1)
for i in range(1, len(number) - window):
    prod = functools.reduce(lambda x, y: x * y, number[i:i + window], 1)
    if prod > max:
        max = prod

print(max)
