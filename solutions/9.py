total = 1000

result = 0

for a in range(1, (total - 1) / 2):
    for b in range(a + 1, total - a):
        c = total - a - b
        if a ** 2 + b ** 2 == c ** 2:
            result = a * b * c

print(result)
