from helpers.sequences import Primes

primes = Primes()
for i in range(0, 10000):
    primes.next()

print(primes.get())
